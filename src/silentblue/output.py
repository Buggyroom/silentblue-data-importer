from collections import ChainMap
from dataclasses import dataclass
from itertools import chain
from typing import Dict, List, Optional

from jinja2 import Environment, PackageLoader, select_autoescape
from more_itertools import windowed

from silentblue import model

KNOWN_CHARTERS = {
    "譜面-100号": "Fumen -100 gou",
    "はっぴー": "Happy",
    "Jack": None,
    "ニャイン": "Nyain",
    "ぴちネコ": "Pichineko",
    "Revo@LC": None,
    "小鳥遊さん": "Takanashi-san",
    "Techno Kitchen": None,
}

VERSION_TAG = {
    model.Version.MAIMAI: "_",
    model.Version.MAIMAI_PLUS: "_+",
    model.Version.GREEN: "green",
    model.Version.GREEN_PLUS: "green+",
    model.Version.ORANGE: "orange",
    model.Version.ORANGE_PLUS: "orange+",
    model.Version.PINK: "pink",
    model.Version.PINK_PLUS: "pink+",
    model.Version.MURASAKI: "murasaki",
    model.Version.MURASAKI_PLUS: "murasaki+",
    model.Version.MILK: "milk",
    model.Version.MILK_PLUS: "milk+",
    model.Version.FINALE: "finale",
    model.Version.DELUXE: "dx",
    model.Version.DELUXE_PLUS: "dx+",
    model.Version.SPLASH: "splash",
    model.Version.SPLASH_PLUS: "splash+",
    model.Version.UNIVERSE: "universe",
    model.Version.UNIVERSE_PLUS: "universe+",
    model.Version.FESTIVAL: "festival",
    model.Version.FESTIVAL_PLUS: "festival+",
    model.Version.BUDDIES: "buddies",
}

def render_song_page(song: model.Song) -> str:
    env = get_jinja_env()
    template = env.get_template("song.txt")
    return template.render(
        song=song,
        level_history=dump_level_history(song),
        KNOWN_CHARTERS=KNOWN_CHARTERS,
        VERSION_TAG=VERSION_TAG,
        model=model,        
    )

def render_difficulty_and_note_counts(song: model.Song) -> str:
    env = get_jinja_env()
    template = env.get_template("difficulty_and_notecounts.txt")
    return template.render(
        song=song,
        level_history=dump_level_history(song),
        KNOWN_CHARTERS=KNOWN_CHARTERS,
        VERSION_TAG=VERSION_TAG,
        model=model,
    )


def render_song_inforation(song: model.Song) -> str:
    env = get_jinja_env()
    template = env.get_template("song_information.txt")
    return template.render(song=song)


@dataclass
class VersionSpanWithLevels:
    first_version: model.Version
    last_version: Optional[model.Version]
    levels: List[Optional[model.Level]]


def dump_level_history(info: model.Song) -> List[VersionSpanWithLevels]:
    level_sets_by_version = group_level_changes_by_version(info)
    table: List[VersionSpanWithLevels] = []
    version_with_changes = sorted(level_sets_by_version.keys())
    version_pairs = windowed(chain(sorted(version_with_changes), [None]), 2)
    for version, next_change in version_pairs:
        if version is None:
            continue
        levels = level_sets_by_version[version]
        if next_change is None:
            last_version = info.last_version
        else:
            last_version = model.Version(next_change - 1)

        table.append(
            VersionSpanWithLevels(
                first_version=version,
                last_version=last_version,
                levels=[levels[d] for d in sorted(levels.keys())],
            )
        )

    return table


def group_level_changes_by_version(
    info: model.Song,
) -> Dict[model.Version, Dict[model.Difficulty, Optional[model.Level]]]:
    versions_with_changes = set.union(
        set(),
        *(
            chart.level.changes.keys()
            for chart in info.standard_charts.values()
            if chart.level is not None
        ),
    )

    first_version = None
    if info.first_version is not None:
        first_version = info.first_version
    elif versions_with_changes:
        first_version = min(versions_with_changes)

    last_version = None
    if info.last_version is not None:
        last_version = info.last_version
    elif versions_with_changes:
        last_version = max(versions_with_changes)

    if first_version is not None and last_version is not None:
        if (
            first_version < model.Version.DELUXE
            and last_version >= model.Version.DELUXE
        ):
            # If song was in both standard and deluxe, add an extra line for deluxe
            # to show that the easy diff was removed
            versions_with_changes.add(model.Version.DELUXE)

    level_sets_by_version: Dict[
        model.Version, Dict[model.Difficulty, Optional[model.Level]]
    ] = {}
    for version in versions_with_changes:
        levels = level_sets_by_version.setdefault(version, {})
        for diff, chart in info.standard_charts.items():
            history = chart.level
            if history is None:
                continue
            if version >= model.Version.DELUXE and diff == model.Difficulty.EASY:
                levels[diff] = None
            else:
                levels[diff] = history.level_at_version(version)

    return level_sets_by_version


SHORT_VERSION_NAME = {
    model.Version.MAIMAI: "",
    model.Version.MAIMAI_PLUS: "PLUS",
    model.Version.GREEN: "GreeN",
    model.Version.GREEN_PLUS: "GreeN PLUS",
    model.Version.ORANGE: "ORANGE",
    model.Version.ORANGE_PLUS: "ORANGE PLUS",
    model.Version.PINK: "PiNK",
    model.Version.PINK_PLUS: "PiNK PLUS",
    model.Version.MURASAKI: "MURASAKi",
    model.Version.MURASAKI_PLUS: "MURASAKi PLUS",
    model.Version.MILK: "MiLK",
    model.Version.MILK_PLUS: "MiLK PLUS",
    model.Version.FINALE: "FiNALE",
    model.Version.DELUXE: "",
    model.Version.DELUXE_PLUS: "PLUS",
    model.Version.SPLASH: "Splash",
    model.Version.SPLASH_PLUS: "Splash PLUS",
    model.Version.UNIVERSE: "UNiVERSE",
    model.Version.UNIVERSE_PLUS: "UNiVERSE PLUS",
    model.Version.FESTIVAL: "FESTiVAL",
    model.Version.FESTIVAL_PLUS: "FESTiVAL PLUS",
    model.Version.BUDDIES: "BUDDiES",
}


def full_version_name(ver: model.Version) -> str:
    if ver < model.Version.DELUXE:
        game = "maimai"
    else:
        game = "maimai でらっくす"
    return f"{game} {SHORT_VERSION_NAME[ver]}".strip()


SPAN_END_VERSION_NAME = ChainMap(
    {
        model.Version.MAIMAI: "maimai",
        model.Version.DELUXE: "でらっくす",
    },
    SHORT_VERSION_NAME,
)


def span_end_version_name(ver: model.Version) -> str:
    return SPAN_END_VERSION_NAME[ver]


JINJA_ENV: Optional[Environment] = None


def get_jinja_env() -> Environment:
    global JINJA_ENV
    if JINJA_ENV is None:
        JINJA_ENV = create_jinja_env()
    return JINJA_ENV


def create_jinja_env() -> Environment:
    env = Environment(
        loader=PackageLoader("silentblue"),
        autoescape=select_autoescape(),
        block_start_string="<%",
        block_end_string="%>",
        variable_start_string="<<",
        variable_end_string=">>",
        comment_start_string="<#",
        comment_end_string="#>",
    )
    env.filters["full_version_name"] = full_version_name
    env.filters["span_end_version_name"] = span_end_version_name
    return env
