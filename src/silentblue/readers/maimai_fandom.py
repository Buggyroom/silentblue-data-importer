"""Reader for maimai.fandom.com"""

import datetime as dt
import re
from dataclasses import dataclass
from decimal import Decimal
from typing import Dict, Iterator, List, Optional, Tuple

from bs4 import BeautifulSoup
from more_itertools import chunked

from silentblue import model
from silentblue.readers import html, utils


def extract_song_info(tree: BeautifulSoup) -> model.Song:
    song_metadata_table = find_song_metadata_table(tree)
    title, artist, bpm = extract_song_metadata(song_metadata_table)
    standard_charts, utage_charts = extract_charts_info(
        find_standard_charts_info_table(tree)
    )
    if utage_charts:
        utage_release_dates = list(extract_utage_release_dates(song_metadata_table))
        for (_, chart), date in zip(utage_charts, utage_release_dates):
            chart.added_in = utils.latest_version_at(date)
    level_history = extract_level_changes(find_level_changes_table(tree))

    charts = {}
    diffs = standard_charts.keys() | level_history.keys()
    for diff in diffs:
        version_levels = level_history.get(diff)
        history = None
        if version_levels is not None:
            history = model.LevelHistory.from_tuples(
                [(vl.version, vl.level) for vl in version_levels]
            )
        chart_info = standard_charts.get(diff)
        note_counts = None
        designer = None
        if chart_info is not None:
            note_counts = chart_info.note_counts
            designer = chart_info.chart_designer

        charts[diff] = model.Chart(
            level=history,
            note_counts=note_counts,
            designer=designer,
        )

    return model.Song(
        title=title,
        artist=artist,
        bpm=bpm,
        standard_charts=charts,
        utage_charts=utage_charts,
    )


SONG_METADATA_TABLE_HEADER = [
    ["", "", "", "", "", "", "", "", "", ""],
    ["樂曲情報"],
]


def find_song_metadata_table(tree: BeautifulSoup) -> List[List[str]]:
    header_text = html.tag(tree.find("b", string="樂曲情報"))
    table_tag = html.tag(header_text.find_parent("table"))
    table = html.parse_table(table_tag)
    if table[:2] != SONG_METADATA_TABLE_HEADER:
        raise ValueError(f"Unexpected song metadata table header : {table}")
    return table


# expected table structure
[
    ["", "", "", "", "", "", "", "", "", ""],
    ["樂曲情報"],
    ["", "樂曲名(JP)", "jelly"],
    ["演唱/作曲(JP)", "capsule [cover]"],
    ["樂曲名(EN)", "jelly"],
    ["演唱/作曲(EN)", "capsule [cover]"],
    ["編號", "42", "類別", "POPS & ANIME(POPS ＆ アニメ)"],
    ["初出版本", "maimai", "追加日期", "2012/07/11", "ID", "jelly"],
    ["BPM", "128", "備註", "?/?/? PV更換 2018/12/25 宴譜面 追加 2019/07/11 DX 版宴会場廢止，變更封面 原封面"],
]


def extract_song_metadata(
    table: List[List[str]],
) -> Tuple[model.RomanizedString, model.RomanizedString, Decimal]:
    info = {}
    for row in table[2:]:
        if len(row) == 3 and row[0] == "":
            info[row[1].strip()] = row[2]
        elif len(row) % 2 == 0:
            for k, v in chunked(row, 2):
                info[k.strip()] = v

    return (
        model.RomanizedString(
            original=info["樂曲名(JP)"].strip(),
            romanized=utils.none_if_empty(info["樂曲名(EN)"].strip()),
        ),
        model.RomanizedString(
            original=info["演唱/作曲(JP)"].strip(),
            romanized=utils.none_if_empty(info["演唱/作曲(EN)"].strip()),
        ),
        Decimal(info["BPM"]),
    )


@dataclass
class NoteCountsAndChartDesigner:
    note_counts: model.NoteCounts
    chart_designer: Optional[str] = None


STANDARD_CHART_INFO_TABLE_HEADER = [
    ["", "", "", "", "", "", "", "", "", ""],
    ["standard 譜面情報"],
]


def find_standard_charts_info_table(tree: BeautifulSoup) -> List[List[str]]:
    header_text = html.tag(tree.find("b", string="standard 譜面情報"))
    table_tag = html.tag(header_text.find_parent("table"))
    table = html.parse_table(table_tag)
    if table[:2] != STANDARD_CHART_INFO_TABLE_HEADER:
        raise ValueError(f"Unexpected standar charts info table header : {table}")
    return table


# expected table structure
[
    ["", "", "", "", "", "", "", "", "", ""],
    ["standard 譜面情報"],
    ["難度", "等級", "COMBO", "TAP", "HOLD", "SLIDE", "BREAK", "理論值", "譜面作者"],
    ["ESY", "2", "68", "58", "5", "2", "3", "100.67%", "-"],
    ["BSC", "5", "105", "95", "6", "3", "1", "100.16%", "-"],
    ["ADV", "6", "118", "88", "21", "7", "2", "100.24%", "-"],
    ["EXP", "10", "210", "139", "55", "14", "2", "100.13%", "譜面-100号"],
    ["MST", "12", "264", "199", "45", "17", "3", "100.16%", "ニャイン"],
    ["ReM", "", "", "", "", "", "", "", ""],
]


def extract_charts_info(
    table: List[List[str]],
) -> Tuple[
    Dict[model.Difficulty, NoteCountsAndChartDesigner],
    List[Tuple[model.UtageDifficulty, model.UtageChart]],
]:
    standard_charts = {}
    utage_charts = []
    keys = table[2]
    for index, row in enumerate(table[3:]):
        info = dict(zip(keys, row))
        raw_diff = info["難度"]
        # utage chart
        if raw_diff.strip() == "宴":
            utage_diff = guess_utage_difficulty(info["等級"])
            utage_charts.append((utage_diff, extract_utage_chart_row_info(info)))
        else:
            diff = utils.guess_difficulty(raw_diff, index)
            # deal with potentially empty re-master row
            if all(utils.is_empty(field) for field in row[1:]):
                continue
            standard_charts[diff] = extract_standard_chart_row_info(info)

    return standard_charts, utage_charts


def guess_utage_difficulty(name: str) -> model.UtageDifficulty:
    return model.UtageDifficulty(name.strip())


def extract_utage_chart_row_info(info: Dict[str, str]) -> model.UtageChart:
    ncd = extract_standard_chart_row_info(info)
    return model.UtageChart(
        note_counts=ncd.note_counts,
        designer=ncd.chart_designer,
        description=None,
        added_in=None,
    )


def extract_standard_chart_row_info(info: Dict[str, str]) -> NoteCountsAndChartDesigner:
    return NoteCountsAndChartDesigner(
        note_counts=extract_note_counts(info),
        chart_designer=extract_chart_designer(info),
    )


def extract_note_counts(info: Dict[str, str]) -> model.NoteCounts:
    return model.NoteCounts(
        total=int(info["COMBO"]),
        taps=int(info["TAP"]),
        holds=int(info["HOLD"]),
        slides=int(info["SLIDE"]),
        breaks=int(info["BREAK"]),
    )


def extract_chart_designer(info: Dict[str, str]) -> Optional[str]:
    return utils.none_if_empty(info["譜面作者"].strip())


STANDARD_LEVEL_CHANGES_TABLE_HEADER = [
    ["", "", "", "", "", "", ""],
    ["standard 譜面等級變更"],
]


def find_level_changes_table(tree: BeautifulSoup) -> List[List[str]]:
    header_text = html.tag(tree.find("b", string="standard 譜面等級變更"))
    table_tag = html.tag(header_text.find_parent("table"))
    table = html.parse_table(table_tag)
    if table[:2] != STANDARD_LEVEL_CHANGES_TABLE_HEADER:
        raise ValueError(f"Unexpected level changes table header : {table}")
    return table


# expected table structure
[
    ["", "", "", "", "", "", ""],
    ["standard 譜面等級變更"],
    ["版本", "ESY", "BSC", "ADV", "EXP", "MST", "ReM"],
    ["maimai", "2", "3", "6", "7", "10", "-"],
    ["maimai+", "2", "3", "5", "8", "9", "-"],
    ["GreeN～", "2", "3", "6", "8", "9", "-"],
    ["ORANGE～", "2", "5", "6", "8", "9", "-"],
    ["PiNK～", "2", "5", "6", "8", "9+", "-"],
    ["MURASAKi～", "2", "5", "6", "8+", "10", "-"],
    ["DX", "-", "5", "6", "8+", "10", "-"],
    ["DX+", "-", "5", "6", "9", "10+", "-"],
    ["Splash～", "-", "5", "6", "9", "11+", "-"],
    ["UNiVERSE+", "-", "5", "6", "10", "12", "-"],
]


def extract_level_changes(
    table: List[List[str]],
) -> Dict[model.Difficulty, List[utils.VersionLevel]]:
    difficulties = [
        utils.guess_difficulty(name, index) for index, name in enumerate(table[2][1:])
    ]
    versions = [guess_version(utils.first_half_of_span(row[0])) for row in table[3:]]
    res: Dict[model.Difficulty, List[utils.VersionLevel]] = {}
    for col, diff in enumerate(difficulties, start=1):
        raw_levels = [row[col] for row in table[3:]]
        if all(utils.is_empty(lvl) for lvl in raw_levels):
            continue
        version_changes = res.setdefault(diff, [])
        for version, raw_level in zip(versions, raw_levels):
            level = utils.read_level(raw_level)
            if level is None:
                continue
            else:
                version_changes.append(utils.VersionLevel(version, level))

    return res


TEXT_TO_VERSION = {
    "maimai": model.Version.MAIMAI,
    "maimai+": model.Version.MAIMAI_PLUS,
    "green": model.Version.GREEN,
    "green+": model.Version.GREEN_PLUS,
    "orange": model.Version.ORANGE,
    "orange+": model.Version.ORANGE_PLUS,
    "pink": model.Version.PINK,
    "pink+": model.Version.PINK_PLUS,
    "murasaki": model.Version.MURASAKI,
    "murasaki+": model.Version.MURASAKI_PLUS,
    "milk": model.Version.MILK,
    "milk+": model.Version.MILK_PLUS,
    "finale": model.Version.FINALE,
    "dx": model.Version.DELUXE,
    "dx+": model.Version.DELUXE_PLUS,
    "splash": model.Version.SPLASH,
    "splash+": model.Version.SPLASH_PLUS,
    "universe": model.Version.UNIVERSE,
    "universe+": model.Version.UNIVERSE_PLUS,
}


def guess_version(raw_name: str) -> model.Version:
    return TEXT_TO_VERSION[raw_name.strip().lower()]


UTAGE_DATE_RE = re.compile(r"(\d{4})/(\d\d)/(\d\d)\s*宴譜面")


def extract_utage_release_dates(
    song_metadata_table: List[List[str]],
) -> Iterator[dt.date]:
    comment_string = song_metadata_table[-1][-1]
    for match in UTAGE_DATE_RE.finditer(comment_string):
        raw_year, raw_month, raw_day = match.groups()
        yield dt.date(
            year=int(raw_year),
            month=int(raw_month),
            day=int(raw_day),
        )
