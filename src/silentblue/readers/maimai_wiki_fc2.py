"""Reader for maimai.wiki.fc2.com, provides the function extract_song_info
which takes in a pre-parsed html page in the form of a BeautifulSoup and
returns a Song instance"""

from collections import defaultdict
from dataclasses import dataclass
from decimal import Decimal
from itertools import chain
from typing import Dict, Iterable, List, Optional, Tuple

from bs4 import BeautifulSoup

from silentblue import model
from silentblue.readers import html, utils


def extract_song_info(tree: BeautifulSoup) -> model.Song:
    song_metadata = find_song_metadata_table(tree)
    title, artist, bpm = extract_song_metadata(song_metadata)
    charts_info = extract_chart_table_info(find_chart_info_table(tree))
    raw_level_changes = []
    standard_level_changes_table = None
    last_version = None
    try:
        standard_level_changes_table = find_full_standard_level_changes_table(tree)
    except ValueError:
        pass

    try:
        standard_level_changes_table = find_old_standard_level_changes_table(tree)
    except ValueError:
        pass
    else:
        last_version = extract_last_version(standard_level_changes_table)

    if standard_level_changes_table is not None:
        raw_level_changes.append(
            extract_standard_level_changes(standard_level_changes_table)
        )
    try:
        deluxe_level_changes_table = find_deluxe_level_changes_table(tree)
    except ValueError:
        pass
    else:
        raw_level_changes.append(
            extract_deluxe_level_changes(deluxe_level_changes_table)
        )

    level_history = merge_level_histories(raw_level_changes)
    first_version = min(
        chain.from_iterable(hist.changes.keys() for hist in level_history.values())
    )

    charts = {}
    for difficulty, level in level_history.items():
        note_counts = None
        chart_designer = None
        if difficulty in charts_info:
            info = charts_info[difficulty]
            note_counts = info.note_counts
            chart_designer = info.chart_designer

        charts[difficulty] = model.Chart(
            level=level,
            note_counts=note_counts,
            designer=chart_designer,
        )

    return model.Song(
        title=title,
        artist=artist,
        bpm=bpm,
        standard_charts=charts,
        first_version=first_version,
        last_version=last_version,
    )


def find_song_metadata_table(tree: BeautifulSoup) -> List[List[str]]:
    song_metadata_header = html.tag(tree.find("h2", string="楽曲情報"))
    next_div = html.tag(song_metadata_header.find_next_sibling("div"))
    table_tag = html.tag(next_div.find("table"))
    return html.parse_table(table_tag)


# expected table structure
[
    ["", "曲名", "Justified"],
    ["アーティスト", "MintJam feat.光吉猛修"],
    ["ジャンル", "maimai"],
    ["BPM", "185"],
    ["追加日", "2018/05/22( チャレンジトラック ) 2018/06/05(通常解禁)"],
    ["MiLK"],
]


def extract_song_metadata(
    table: List[List[str]],
) -> Tuple[model.RomanizedString, model.RomanizedString, Decimal]:
    info = {}
    for line in table:
        if len(line) <= 1:
            continue
        elif len(line) == 2:
            info[line[0].strip()] = line[1]
        elif len(line) == 3 and line[0] == "":
            info[line[1].strip()] = line[2]

    return (
        model.RomanizedString(original=info["曲名"], romanized=None),
        model.RomanizedString(original=info["アーティスト"], romanized=None),
        Decimal(info["BPM"]),
    )


CHART_TABLE_HEADER = [
    ["難易度", "レベル", "ノーツ総数", "内訳", "譜面制作者"],
    ["TAP", "HOLD", "SLIDE", "BREAK"],
]


def find_chart_info_table(tree: BeautifulSoup) -> List[List[str]]:
    chart_info_header = html.tag(tree.find("h2", string="譜面情報"))
    next_div = html.tag(chart_info_header.find_next_sibling("div"))
    table_tag = html.tag(next_div.find("table"))
    table = html.parse_table(table_tag)
    if table[:2] != CHART_TABLE_HEADER:
        raise ValueError(f"Unexpected table header : {table[:2]}")
    return table


@dataclass
class NoteCountsAndChartDesigner:
    note_counts: model.NoteCounts
    chart_designer: Optional[str] = None


def extract_chart_table_info(
    chart_info_table: List[List[str]],
) -> Dict[model.Difficulty, NoteCountsAndChartDesigner]:
    res = {}
    for index, row in enumerate(chart_info_table[2:]):
        raw_diff_name = row[0]
        diff = utils.guess_difficulty(raw_diff_name, index)
        res[diff] = extract_row_info(row)

    return res


def extract_row_info(row: List[str]) -> NoteCountsAndChartDesigner:
    return NoteCountsAndChartDesigner(
        note_counts=extract_note_counts(row),
        chart_designer=extract_chart_designer(row[7]),
    )


def extract_note_counts(row: List[str]) -> model.NoteCounts:
    return model.NoteCounts(
        total=int(row[2]),
        taps=int(row[3]),
        holds=int(row[4]),
        slides=int(row[5]),
        breaks=int(row[6]),
    )


def extract_chart_designer(raw: str) -> Optional[str]:
    s = raw.strip()
    if s in ("", "-"):
        return None

    return s


def find_full_standard_level_changes_table(tree: BeautifulSoup) -> List[List[str]]:
    """Finds the table in a collapsable element, after a header named レベル変更履歴"""
    level_info_header = html.tag(tree.find("h2", string="レベル変更履歴"))
    next_div = html.tag(level_info_header.find_next_sibling("div"))
    if next_div["class"] != ["region"]:
        raise ValueError(f"Unexpected div class after h2 header : {next_div['class']}")
    table_tag = html.tag(next_div.find("table"))
    return html.parse_table(table_tag)


def find_old_standard_level_changes_table(tree: BeautifulSoup) -> List[List[str]]:
    """Find the table directly after a header named 旧レベル"""
    level_info_header = html.tag(tree.find("h2", string="旧レベル"))
    next_div = html.tag(level_info_header.find_next_sibling("div"))
    table_tag = html.tag(next_div.find("table"))
    return html.parse_table(table_tag)


STANDARD_LEVEL_CHANGES_TABLE_FINDERS = [
    find_full_standard_level_changes_table,
    find_old_standard_level_changes_table,
]


def extract_last_version(table: List[List[str]]) -> model.Version:
    last_version_span = table[0][-1]
    return guess_version(utils.second_half_of_span(last_version_span))


def extract_standard_level_changes(
    table: List[List[str]],
) -> Dict[model.Difficulty, List[utils.VersionLevel]]:
    version_spans = table[0][1:]
    versions = [guess_version(utils.first_half_of_span(span)) for span in version_spans]
    res: Dict[model.Difficulty, List[utils.VersionLevel]] = {}
    for index, row in enumerate(table[1:]):
        diff = utils.guess_difficulty(row[0], index)
        version_changes = res.setdefault(diff, [])
        for version, raw_level in zip(versions, row[1:]):
            level = utils.read_level(raw_level)
            if level is None:
                continue
            else:
                version_changes.append(utils.VersionLevel(version, level))

    return res


TEXT_TO_VERSION = {
    "無印": model.Version.MAIMAI,
    "plus": model.Version.MAIMAI_PLUS,
    "green": model.Version.GREEN,
    "green+": model.Version.GREEN_PLUS,
    "orange": model.Version.ORANGE,
    "orange+": model.Version.ORANGE_PLUS,
    "pink": model.Version.PINK,
    "pink+": model.Version.PINK_PLUS,
    "murasaki": model.Version.MURASAKI,
    "murasaki+": model.Version.MURASAKI_PLUS,
    "milk": model.Version.MILK,
    "milk+": model.Version.MILK_PLUS,
    "finale": model.Version.FINALE,
    "でらっくす": model.Version.DELUXE,
    "でらっくす plus": model.Version.DELUXE_PLUS,
    "でらっくす+": model.Version.DELUXE_PLUS,
    "splash": model.Version.SPLASH,
    "splash plus": model.Version.SPLASH_PLUS,
    "splash+": model.Version.SPLASH_PLUS,
}


def guess_version(raw_name: str) -> model.Version:
    return TEXT_TO_VERSION[raw_name.lower().strip()]


DELUXE_LEVEL_TABLE_HEADER = ["バージョン", "スタンダード譜面"]


def find_deluxe_level_changes_table(tree: BeautifulSoup) -> List[List[str]]:
    level_info_header = html.tag(tree.find("h2", string="レベル変更履歴"))
    second_div = html.tag(level_info_header.find_next_siblings("div")[1])
    table_tag = html.tag(second_div.find("table"))
    table = html.parse_table(table_tag)
    if table[0] != DELUXE_LEVEL_TABLE_HEADER:
        raise ValueError(f"Unexpected table header : {table[0]}")
    return table


# expected table structure example :
[
    ["バージョン", "スタンダード譜面"],
    ["B", "A", "E", "M", "R"],
    ["でらっくす～", "5", "6", "8+", "10", "-"],
    ["でらっくす PLUS～", "5", "6", "9", "10+", "-"],
]


def extract_deluxe_level_changes(
    table: List[List[str]],
) -> Dict[model.Difficulty, List[utils.VersionLevel]]:
    difficulties = [
        utils.guess_difficulty(name, index) for index, name in enumerate(table[1])
    ]
    versions = [guess_version(utils.first_half_of_span(row[0])) for row in table[2:]]
    res: Dict[model.Difficulty, List[utils.VersionLevel]] = {}
    for col, diff in enumerate(difficulties, start=1):
        raw_levels = [row[col] for row in table[2:]]
        version_changes = res.setdefault(diff, [])
        for version, raw_level in zip(versions, raw_levels):
            level = utils.read_level(raw_level)
            if level is None:
                continue
            else:
                version_changes.append(utils.VersionLevel(version, level))

    return res


def merge_level_histories(
    histories: Iterable[Dict[model.Difficulty, List[utils.VersionLevel]]]
) -> Dict[model.Difficulty, model.LevelHistory]:
    merged = defaultdict(list)
    for diff in model.Difficulty:
        for history in histories:
            merged[diff].extend(history.get(diff, []))

    empty_diffs = [
        diff for diff, version_levels in merged.items() if not version_levels
    ]
    for diff in empty_diffs:
        del merged[diff]

    return {
        diff: model.LevelHistory.from_tuples(
            [(vl.version, vl.level) for vl in version_levels]
        )
        for diff, version_levels in merged.items()
    }
