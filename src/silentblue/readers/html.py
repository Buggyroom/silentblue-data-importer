from typing import Any, List

from bs4 import BeautifulSoup, Tag


def read(a: Any) -> BeautifulSoup:
    return BeautifulSoup(a, "lxml")


def parse_table(t: Tag) -> List[List[str]]:
    "Parse an html <table> into a grid of strings"
    res = []
    for tr in t.find_all("tr"):
        res.append(
            [" ".join(cell.stripped_strings) for cell in tr.find_all(["th", "td"])]
        )

    return res


def tag(a: Any) -> Tag:
    if not isinstance(a, Tag):
        raise ValueError(f"Unexpected object type : {a}")
    else:
        return a
