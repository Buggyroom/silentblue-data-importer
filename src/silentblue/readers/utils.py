import datetime as dt
from dataclasses import dataclass
from typing import Optional

from silentblue import model

# This most notably does not have song.Difficulty.EASY, which is handled
# separately by guess_difficulty
LETTER_TO_DIFFICULTY = {
    "b": model.Difficulty.BASIC,
    "a": model.Difficulty.ADVANCED,
    "e": model.Difficulty.EXPERT,
    "m": model.Difficulty.MASTER,
    "r": model.Difficulty.RE_MASTER,
}


def guess_difficulty(raw_name: str, index: int) -> model.Difficulty:
    name = raw_name.strip().lower()
    if name[0] == "e" and index == 0:
        return model.Difficulty.EASY
    else:
        return LETTER_TO_DIFFICULTY[name[0]]


def read_level(raw: str) -> Optional[model.Level]:
    if is_empty(raw):
        return None
    else:
        return model.Level.from_string(raw)


def none_if_empty(text: str) -> Optional[str]:
    """Detects strings conscidered 'empty' and turns them into None"""
    if is_empty(text):
        return None
    else:
        return text


def is_empty(text: str) -> bool:
    return text.strip() in ("", "-")


@dataclass
class VersionLevel:
    version: model.Version
    level: model.Level


FULLWIDTH_TILDE = "\uff5e"


def first_half_of_span(span: str) -> str:
    return span.split(FULLWIDTH_TILDE)[0]


def second_half_of_span(span: str) -> str:
    """Return the "second" half of a version span.
    >>> second_half_of_span("a〜b")
    "b"
    >>> second_half_of_span("a")
    "a"
    """
    return span.split(FULLWIDTH_TILDE)[-1]


RELEASE_DATES = {
    model.Version.MAIMAI: dt.date(2012, 7, 11),
    model.Version.MAIMAI_PLUS: dt.date(2012, 12, 13),
    model.Version.GREEN: dt.date(2013, 7, 11),
    model.Version.GREEN_PLUS: dt.date(2014, 2, 26),
    model.Version.ORANGE: dt.date(2014, 9, 18),
    model.Version.ORANGE_PLUS: dt.date(2015, 3, 19),
    model.Version.PINK: dt.date(2015, 12, 9),
    model.Version.PINK_PLUS: dt.date(2016, 6, 30),
    model.Version.MURASAKI: dt.date(2016, 12, 15),
    model.Version.MURASAKI_PLUS: dt.date(2017, 6, 22),
    model.Version.MILK: dt.date(2017, 12, 14),
    model.Version.MILK_PLUS: dt.date(2018, 6, 21),
    model.Version.FINALE: dt.date(2018, 12, 13),
    model.Version.DELUXE: dt.date(2019, 7, 11),
    model.Version.DELUXE_PLUS: dt.date(2020, 1, 23),
    model.Version.SPLASH: dt.date(2020, 9, 17),
    model.Version.SPLASH_PLUS: dt.date(2021, 3, 18),
    model.Version.UNIVERSE: dt.date(2021, 9, 16),
    model.Version.UNIVERSE_PLUS: dt.date(2022, 3, 24),
    model.Version.FESTIVAL: dt.date(2022, 9, 15),
    model.Version.FESTIVAL_PLUS: dt.date(2023, 3, 23),
    model.Version.BUDDIES: dt.date(2023, 9, 14)
}


def latest_version_at(date: dt.date) -> model.Version:
    for version, release_date in reversed(RELEASE_DATES.items()):
        if date >= release_date:
            return version

    raise ValueError(f"Maimai had not been released yet on {date}")
