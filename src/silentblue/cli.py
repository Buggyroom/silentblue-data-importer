import re
import sys
import traceback
import warnings
from contextlib import contextmanager
from functools import wraps
from typing import IO, Callable, Iterator, List, Optional, Type

import click
import requests
from bs4 import BeautifulSoup

from silentblue import model, output
from silentblue.readers import html, maimai_fandom, maimai_wiki_fc2

URL_MAP = {
    re.compile(r"maimai\.wiki\.fc2\.com"): maimai_wiki_fc2.extract_song_info,
    re.compile(r"maimai\.fandom\.com"): maimai_fandom.extract_song_info,
}


def find_reader(url: str) -> Callable[[BeautifulSoup], model.Song]:
    for regex, reader in URL_MAP.items():
        match = regex.search(url)
        if match is not None:
            return reader

    raise ValueError(f"No reader found for url {url}")


def capture_warnings(f: Callable) -> Callable:
    @wraps(f)
    def wrapped(*a, **kw):
        with set_warnings_callback():
            return f(*a, **kw)

    return wrapped


@contextmanager
def set_warnings_callback():
    old_callback = warnings.showwarning
    warnings.showwarning = print_warning_in_yellow
    try:
        yield None
    finally:
        warnings.showwarning = old_callback


def print_warning_in_yellow(
    message: str,
    category: Type[Warning],
    filename: str,
    lineno: int,
    file: Optional[IO] = None,
    line: Optional[str] = None,
) -> None:
    text = warnings.formatwarning(message, category, filename, lineno, line)
    click.secho(text, fg="yellow", file=file)


@capture_warnings
@click.command()
@click.argument("urls", nargs=-1)
@click.option("-v", "--verbose", count=True)
def scrape_chart_info(urls: List[str], verbose: int):
    songs = []
    for url in urls:
        with capture_exceptions(url, verbose):
            song = scrape_single_page(url)
            songs.append(song)

    if not songs:
        click.secho("Couldn't get info from any of the sources, aborting", fg="red")
        sys.exit(1)

    final_song = model.Song.merge(songs)
    click.echo(output.render_song_inforation(final_song))
    click.echo(output.render_difficulty_and_note_counts(final_song))


@contextmanager
def capture_exceptions(url: str, verbose: int) -> Iterator[None]:
    try:
        yield None
    except Exception as e:
        if verbose:
            click.secho(f"Error during scraping of {url} :\n", fg="red")
            click.secho(traceback.format_exc(), fg="red")
        else:
            click.secho(f"{e.__class__.__name__} during scraping of {url}", fg="red")

        click.secho("Skipping ...", fg="red")


def scrape_single_page(url: str) -> model.Song:
    reader = find_reader(url)
    req = requests.get(url)
    req.raise_for_status()
    tree = html.read(req.content)
    return reader(tree)


if __name__ == "__main__":
    scrape_chart_info()
