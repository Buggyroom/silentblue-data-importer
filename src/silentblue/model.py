import pprint
import warnings
from collections import ChainMap
from dataclasses import dataclass, field
from decimal import Decimal
from enum import Enum, IntEnum, auto
from typing import Dict, Iterable, List, Optional, Tuple


# Use IntEnum to be able to compare and sort
class Difficulty(IntEnum):
    EASY = auto()
    BASIC = auto()
    ADVANCED = auto()
    EXPERT = auto()
    MASTER = auto()
    RE_MASTER = auto()


class UtageDifficulty(str, Enum):
    TILT = "傾"  # The normal chart, but rotated
    LIGHT = "光"  # Every note becomes a break note
    COOPERATION = "協"  # Chart for 2+ players on one screen
    IMMEDIATELY = "即"  # Slides start faster
    PARTY = "宴"  # Generic utage diff, Pattern is changed for another concept
    STROKE = "撫"  # Slides without tap stars
    STAR = "星"  # Regular tap notes become stars
    INSANE = "狂"  # Very hard chart or clear conditions
    TOLERANCE = "耐"  # Tests a specific skill
    WAREHOUSE = "蔵"  # Beta version of a chart
    OCTOPUS = "蛸"  # 2+ notes at the same time
    MEMORIZE = "覚"  # Slide patterns you have to memorize
    REVERSE = "逆"  # Moves or adds star taps at the end of slides


@dataclass(frozen=True)
class NoteCounts:
    total: int
    taps: int
    holds: int
    slides: int
    breaks: int

    def __post_init__(self):
        if not self.total_is_consistent():
            warnings.warn(
                f"Inconsistent total note count : given total is {self.total} "
                f"while detailed counts indicate it should be {self.recompute_total()}"
            )

    def total_is_consistent(self) -> bool:
        return self.total == self.recompute_total()

    def recompute_total(self) -> int:
        return self.taps + self.holds + self.slides + self.breaks


@dataclass
class Level:
    number: int
    plus: bool

    def __str__(self) -> str:
        plus_sign = "+" if self.plus else ""
        return f"{self.number}{plus_sign}"

    @classmethod
    def from_string(cls, raw: str):
        s = raw.strip()
        return cls(number=int(s.rstrip("+")), plus=s.endswith("+"))


# Use IntEnum to be able to compare and sort
class Version(IntEnum):
    MAIMAI = auto()
    MAIMAI_PLUS = auto()
    GREEN = auto()
    GREEN_PLUS = auto()
    ORANGE = auto()
    ORANGE_PLUS = auto()
    PINK = auto()
    PINK_PLUS = auto()
    MURASAKI = auto()
    MURASAKI_PLUS = auto()
    MILK = auto()
    MILK_PLUS = auto()
    FINALE = auto()
    DELUXE = auto()
    DELUXE_PLUS = auto()
    SPLASH = auto()
    SPLASH_PLUS = auto()
    UNIVERSE = auto()
    UNIVERSE_PLUS = auto()
    FESTIVAL = auto()
    FESTIVAL_PLUS = auto()
    BUDDIES = auto()


@dataclass
class LevelHistory:
    changes: Dict[Version, Level]

    def update(self, other: "LevelHistory") -> None:
        self.changes.update(other.changes)
        history: List[Tuple[Version, Level]] = []
        for version in Version:
            if version not in self.changes:
                continue
            else:
                level = self.changes[version]
                if history and history[-1][1] == level:
                    continue
                else:
                    history.append((version, level))

        self.changes = dict(history)

    @classmethod
    def from_tuples(cls, changes: Iterable[Tuple[Version, Level]]) -> "LevelHistory":
        history: List[Tuple[Version, Level]] = []
        for version, level in sorted(changes):
            if history and history[-1][1] == level:
                continue
            else:
                history.append((version, level))

        return cls(changes=dict(history))

    @classmethod
    def merge(cls, histories: List["LevelHistory"]) -> "LevelHistory":
        return cls.from_tuples(
            ChainMap(*(h.changes for h in reversed(histories))).items()
        )

    def level_at_version(self, version: Version) -> Optional[Level]:
        if version in self.changes:
            return self.changes[version]

        # search backwards
        previous_version = version
        while True:
            try:
                previous_version = Version(previous_version - 1)
            except ValueError:
                return None
            if previous_version in self.changes:
                return self.changes[previous_version]


@dataclass
class Chart:
    level: Optional[LevelHistory]
    note_counts: Optional[NoteCounts]
    designer: Optional[str]


@dataclass
class UtageChart:
    note_counts: Optional[NoteCounts]
    designer: Optional[str]
    description: Optional[str]
    added_in: Optional[Version]


@dataclass
class RomanizedString:
    original: str
    romanized: Optional[str]

    def different_when_romanized(self):
        return self.romanized is not None and self.original != self.romanized



@dataclass
class Song:
    title: RomanizedString
    artist: RomanizedString
    bpm: Decimal
    standard_charts: Dict[Difficulty, Chart]
    utage_charts: List[Tuple[UtageDifficulty, UtageChart]] = field(default_factory=list)
    first_version: Optional[Version] = None
    last_version: Optional[Version] = None

    @classmethod
    def merge(cls, songs: List["Song"]) -> "Song":
        return cls(
            title=merge_titles(songs),
            artist=merge_artists(songs),
            bpm=merge_bpms(songs),
            standard_charts=merge_standard_charts(songs),
            utage_charts=merge_utage_charts(songs),
            first_version=merge_first_versions(songs),
            last_version=merge_last_versions(songs),
        )


def merge_titles(songs: List[Song]) -> RomanizedString:
    originals = set(song.title.original for song in songs)
    if len(originals) > 1:
        warnings.warn(
            f"Sources disagree on the original song title :\n"
            f"{originals}\n"
            "Defaulting to the first one recieved"
        )
    original = songs[0].title.original
    romanized_titles = set(song.title.romanized for song in songs)
    romanized_titles -= {None}
    if len(romanized_titles) > 1:
        warnings.warn(
            f"Sources disagree on the romanized song title:\n"
            f"{romanized_titles}\n"
            "Defaulting to the first non-empty one recieved"
        )
    romanized = None
    if romanized_titles:
        romanized = next(
            song.title.romanized for song in songs if song.title.romanized is not None
        )
    return RomanizedString(
        original=original,
        romanized=romanized,
    )


def merge_artists(songs: List[Song]) -> RomanizedString:
    originals = set(song.artist.original for song in songs)
    if len(originals) > 1:
        warnings.warn(
            f"Sources disagree on the original song artist :\n"
            f"{originals}\n"
            "Defaulting to the first one recieved"
        )
    original = songs[0].artist.original
    romanized_artists = set(song.artist.romanized for song in songs)
    romanized_artists -= {None}
    if len(romanized_artists) > 1:
        warnings.warn(
            f"Sources disagree on the romanized song artist:\n"
            f"{romanized_artists}\n"
            "Defaulting to the first non-empty one recieved"
        )
    romanized = None
    if romanized_artists:
        romanized = next(
            song.artist.romanized for song in songs if song.artist.romanized is not None
        )
    return RomanizedString(
        original=original,
        romanized=romanized,
    )


def merge_bpms(songs: List[Song]) -> Decimal:
    bpms = set(song.bpm for song in songs)
    if len(bpms) > 1:
        warnings.warn(
            f"Sources disagree on the bpm:\n"
            f"{bpms}\n"
            "Defaulting to the first one recieved"
        )
    return songs[0].bpm


def merge_standard_charts(songs: List[Song]) -> Dict[Difficulty, Chart]:
    levels_by_diff: Dict[Difficulty, List[LevelHistory]] = {}
    note_counts_by_diff: Dict[Difficulty, List[NoteCounts]] = {}
    designer_by_diff: Dict[Difficulty, List[str]] = {}
    for song in songs:
        for diff, chart in song.standard_charts.items():
            if chart.level is not None:
                levels_by_diff.setdefault(diff, []).append(chart.level)
            if chart.note_counts is not None:
                note_counts_by_diff.setdefault(diff, []).append(chart.note_counts)
            if chart.designer is not None:
                designer_by_diff.setdefault(diff, []).append(chart.designer)

    diffs = set.union(set(), *(song.standard_charts.keys() for song in songs))
    charts = {}
    for diff in diffs:
        level = LevelHistory.merge(levels_by_diff.get(diff, []))
        all_note_counts = note_counts_by_diff.get(diff, [])
        unique_note_counts = set(all_note_counts)
        if len(unique_note_counts) > 1:
            warnings.warn(
                f"Found different note counts for {diff._name_} chart :\n"
                f"{pprint.pformat(unique_note_counts)}\n"
                "Defaulting to the last one recieved"
            )
        chosen_note_counts = None
        if all_note_counts:
            chosen_note_counts = all_note_counts[-1]
        all_designers = designer_by_diff.get(diff, [])
        unique_designers = set(all_designers)
        if len(unique_designers) > 1:
            warnings.warn(
                f"Founds different designers for {diff._name_} chart : "
                f"{all_designers}\n"
                "Defaulting to the last one recieved"
            )
        chosen_designer = None
        if all_designers:
            chosen_designer = all_designers[-1]

        charts[diff] = Chart(
            level=level,
            note_counts=chosen_note_counts,
            designer=chosen_designer,
        )

    return charts


def merge_utage_charts(
    songs: List[Song],
) -> List[Tuple[UtageDifficulty, UtageChart]]:
    all_charts_by_diff: Dict[UtageDifficulty, List[List[UtageChart]]] = {}
    for song in songs:
        charts_by_diff: Dict[UtageDifficulty, List[UtageChart]] = {}
        for diff, chart in song.utage_charts:
            charts_by_diff.setdefault(diff, []).append(chart)

        for diff_, charts in charts_by_diff.items():
            all_charts_by_diff.setdefault(diff_, []).append(charts)

    res = []
    for diff, chart_sets in all_charts_by_diff.items():
        chart_sets_sizes = set(len(charts) for charts in chart_sets)
        if chart_sets_sizes == {1}:
            res.append((diff, chart_sets[0][0]))
        elif len(chart_sets_sizes) == 1:
            warnings.warn(
                f"This song has {len(chart_sets_sizes)} utage charts of type "
                f"{diff}, the code does NOT try to cross-compare their info in "
                "any clever way for now and just uses the values from the "
                "first source"
            )
            res.extend((diff, chart) for chart in chart_sets[0])
        else:
            warnings.warn(
                "Sources disagree on the numbers of utage charts of type "
                f"{diff} for this song, defaulting to the source with the "
                "biggest list"
            )
            biggest = max(chart_sets, key=len)
            res.extend((diff, chart) for chart in biggest)

    return res


def merge_first_versions(songs: List[Song]) -> Optional[Version]:
    all_first_versions = set(
        s.first_version for s in songs if s.first_version is not None
    )
    if len(all_first_versions) > 1:
        warnings.warn(
            "Sources disagree on the version this song appeared in,"
            "defaulting to the earliest one"
        )
    return min(all_first_versions, default=None)


def merge_last_versions(songs: List[Song]) -> Optional[Version]:
    all_last_versions = set(s.last_version for s in songs if s.last_version is not None)
    if len(all_last_versions) > 1:
        warnings.warn(
            "Sources disagree on the last version this song was featured in,"
            "defaulting to the latest one"
        )
    return max(all_last_versions, default=None)
