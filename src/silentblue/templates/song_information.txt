{{DISPLAYTITLE:<< song.title.original >>}}
== Song Information ==
[[Image:<<jacket_path>>|thumb|<< song.title.original >>'s maimai jacket.]]
Artist: << song.artist.original >><br>
Composition/Lyrics: ?<br>
Movie: ?<br>
BPM: << song.bpm >><br>
Length: ?<br>
maimai Genre: ?<br>
First Music Game Appearance: ?<br>
Other Music Game Appearances: ?<br>

== Lyrics ==
{{Need lyrics}}

== Song Connections / Remixes ==
None.

== Trivia ==
<% set rom_title = song.title.different_when_romanized() -%>
<%- set rom_artist = song.artist.different_when_romanized() -%>
<%- if rom_title and not rom_artist -%>
    * In Asian English versions of maimai, << song.title.original >>'s title was romanised as '''<< song.title.romanized >>'''.
<%- elif not rom_title and rom_artist -%>
    * In Asian English versions of maimai, << song.title.original >>'s artist was romanised as '''<< song.artist.romanized >>'''.
<%- elif rom_title and rom_artist -%>
    * In Asian English versions of maimai, << song.title.original >>'s title and artist were romanised as '''<< song.title.romanized >>''' and '''<< song.artist.romanized >>'''.
<%- endif -%>

== Genre Changes ==
None.

