from decimal import Decimal
from importlib import resources
from textwrap import dedent

from silentblue import model, output
from silentblue.readers import html, maimai_wiki_fc2
from silentblue.tests.examples import maimai_wiki_fc2 as examples


def test_example_come_again():
    with resources.open_binary(examples, "come_again.html") as f:
        tree = html.read(f)

    found = maimai_wiki_fc2.extract_song_info(tree)
    expected = model.Song(
        title=model.RomanizedString("come again", None),
        artist=model.RomanizedString("m-flo [cover]", None),
        bpm=Decimal("130"),
        standard_charts={
            model.Difficulty.EASY: model.Chart(
                level=model.LevelHistory({model.Version.MAIMAI: model.Level(2, False)}),
                note_counts=None,
                designer=None,
            ),
            model.Difficulty.BASIC: model.Chart(
                level=model.LevelHistory(
                    {
                        model.Version.MAIMAI: model.Level(3, False),
                        model.Version.ORANGE: model.Level(5, False),
                    }
                ),
                note_counts=model.NoteCounts(105, 95, 6, 3, 1),
                designer=None,
            ),
            model.Difficulty.ADVANCED: model.Chart(
                level=model.LevelHistory(
                    {
                        model.Version.MAIMAI: model.Level(6, False),
                        model.Version.MAIMAI_PLUS: model.Level(5, False),
                        model.Version.GREEN: model.Level(6, False),
                    }
                ),
                note_counts=model.NoteCounts(118, 88, 21, 7, 2),
                designer=None,
            ),
            model.Difficulty.EXPERT: model.Chart(
                level=model.LevelHistory(
                    {
                        model.Version.MAIMAI: model.Level(7, False),
                        model.Version.MAIMAI_PLUS: model.Level(8, False),
                        model.Version.MURASAKI: model.Level(8, True),
                        model.Version.DELUXE_PLUS: model.Level(9, False),
                    }
                ),
                note_counts=model.NoteCounts(210, 139, 55, 14, 2),
                designer="譜面-100号",
            ),
            model.Difficulty.MASTER: model.Chart(
                level=model.LevelHistory(
                    {
                        model.Version.MAIMAI: model.Level(10, False),
                        model.Version.MAIMAI_PLUS: model.Level(9, False),
                        model.Version.PINK: model.Level(9, True),
                        model.Version.MURASAKI: model.Level(10, False),
                        model.Version.DELUXE_PLUS: model.Level(10, True),
                    }
                ),
                note_counts=model.NoteCounts(264, 199, 45, 17, 3),
                designer="ニャイン",
            ),
        },
        first_version=model.Version.MAIMAI,
        last_version=None,
    )
    assert found == expected

    dumped = output.render_song_page(found)
    assert (
        dumped
        == dedent(
            """
            {{DISPLAYTITLE:come again}}
            == Song Information ==
            [[Image:|thumb|come again's maimai jacket.]]
            Artist: m-flo [cover]<br>
            Composition/Lyrics: ?<br>
            Movie: ?<br>
            BPM: 130<br>
            Length: ?<br>
            maimai Genre: ?<br>
            First Music Game Appearance: ?<br>
            Other Music Game Appearances: ?<br>

            == Lyrics ==
            {{Need lyrics}}

            == Song Connections / Remixes ==
            None.

            == Trivia ==
            == Genre Changes ==
            None.

            == Difficulty & Notecounts ==
            {{maimai Difficulty|_}}
            {{Notecount Sources}}

            === maimai ===
            ==== Standard Charts ====
            {{maimai Chart Header|
            {{maimai Chart Notecounts||105|118|210|264}}
            {{maimai Chart Breakdown| /  /  / |95 / 6 / 3 / 1|88 / 21 / 7 / 2|139 / 55 / 14 / 2|199 / 45 / 17 / 3}}
            {{maimai Chart Designer|[[Fumen -100 gou|譜面-100号]]|[[Nyain|ニャイン]]}}
            {{maimai Chart|maimai|2|3|6|7|10}}
            {{maimai Chart|maimai PLUS|2|3|5|8|9}}
            {{maimai Chart|maimai GreeN&rarr;GreeN PLUS|2|3|6|8|9}}
            {{maimai Chart|maimai ORANGE&rarr;ORANGE PLUS|2|5|6|8|9}}
            {{maimai Chart|maimai PiNK&rarr;PiNK PLUS|2|5|6|8|9+}}
            {{maimai Chart|maimai MURASAKi&rarr;FiNALE|2|5|6|8+|10}}
            {{maimai Chart|maimai でらっくす|-|5|6|8+|10}}
            {{maimai Chart|maimai でらっくす PLUS&rarr;Present|-|5|6|9|10+}}
            }}

            [[Category:?]]
            """
        ).strip()
    )


def test_example_future():
    with resources.open_binary(examples, "future.html") as f:
        tree = html.read(f)

    found = maimai_wiki_fc2.extract_song_info(tree)
    expected = model.Song(
        title=model.RomanizedString("Future", None),
        artist=model.RomanizedString("★STAR GUiTAR [cover]", None),
        bpm=Decimal("130"),
        standard_charts={
            model.Difficulty.EASY: model.Chart(
                level=model.LevelHistory(
                    changes={
                        model.Version.MAIMAI: model.Level(number=3, plus=False),
                        model.Version.MAIMAI_PLUS: model.Level(number=2, plus=False),
                        model.Version.ORANGE: model.Level(number=3, plus=False),
                    }
                ),
                note_counts=None,
                designer=None,
            ),
            model.Difficulty.BASIC: model.Chart(
                level=model.LevelHistory(
                    changes={
                        model.Version.MAIMAI: model.Level(number=5, plus=False),
                        model.Version.ORANGE: model.Level(number=6, plus=False),
                    }
                ),
                note_counts=model.NoteCounts(
                    total=188, taps=183, holds=0, slides=3, breaks=2
                ),
                designer=None,
            ),
            model.Difficulty.ADVANCED: model.Chart(
                level=model.LevelHistory(
                    changes={model.Version.MAIMAI: model.Level(number=6, plus=False)}
                ),
                note_counts=model.NoteCounts(
                    total=210, taps=178, holds=16, slides=12, breaks=4
                ),
                designer=None,
            ),
            model.Difficulty.EXPERT: model.Chart(
                level=model.LevelHistory(
                    changes={
                        model.Version.MAIMAI: model.Level(number=8, plus=False),
                        model.Version.PINK: model.Level(number=7, plus=True),
                        model.Version.DELUXE_PLUS: model.Level(number=8, plus=False),
                    }
                ),
                note_counts=model.NoteCounts(
                    total=304, taps=284, holds=12, slides=5, breaks=3
                ),
                designer="maimai TEAM",
            ),
            model.Difficulty.MASTER: model.Chart(
                level=model.LevelHistory(
                    changes={
                        model.Version.MAIMAI: model.Level(number=10, plus=False),
                        model.Version.MAIMAI_PLUS: model.Level(number=9, plus=False),
                        model.Version.GREEN: model.Level(number=8, plus=False),
                        model.Version.PINK: model.Level(number=9, plus=False),
                        model.Version.DELUXE_PLUS: model.Level(number=10, plus=False),
                    }
                ),
                note_counts=model.NoteCounts(
                    total=286, taps=226, holds=18, slides=33, breaks=9
                ),
                designer="譜面-100号",
            ),
            model.Difficulty.RE_MASTER: model.Chart(
                level=model.LevelHistory(
                    changes={
                        model.Version.ORANGE_PLUS: model.Level(number=11, plus=False),
                        model.Version.DELUXE_PLUS: model.Level(number=11, plus=True),
                    }
                ),
                note_counts=model.NoteCounts(
                    total=496, taps=238, holds=4, slides=128, breaks=126
                ),
                designer="はっぴー",
            ),
        },
        first_version=model.Version.MAIMAI,
        last_version=None,
    )
    assert found == expected

    dumped = output.render_song_page(found)
    assert (
        dumped
        == dedent(
            """
            {{DISPLAYTITLE:Future}}
            == Song Information ==
            [[Image:|thumb|Future's maimai jacket.]]
            Artist: ★STAR GUiTAR [cover]<br>
            Composition/Lyrics: ?<br>
            Movie: ?<br>
            BPM: 130<br>
            Length: ?<br>
            maimai Genre: ?<br>
            First Music Game Appearance: ?<br>
            Other Music Game Appearances: ?<br>

            == Lyrics ==
            {{Need lyrics}}

            == Song Connections / Remixes ==
            None.

            == Trivia ==
            == Genre Changes ==
            None.

            == Difficulty & Notecounts ==
            {{maimai Difficulty|_}}
            {{Notecount Sources}}

            === maimai ===
            ==== Standard Charts ====
            {{maimai Chart Header|
            {{maimai Chart Notecounts||188|210|304|286|496}}
            {{maimai Chart Breakdown| /  /  / |183 / 0 / 3 / 2|178 / 16 / 12 / 4|284 / 12 / 5 / 3|226 / 18 / 33 / 9|238 / 4 / 128 / 126}}
            {{maimai Chart Designer|maimai TEAM|[[Fumen -100 gou|譜面-100号]]|[[Happy|はっぴー]]}}
            {{maimai Chart|maimai|3|5|6|8|10|-}}
            {{maimai Chart|maimai PLUS|2|5|6|8|9|-}}
            {{maimai Chart|maimai GreeN&rarr;GreeN PLUS|2|5|6|8|8|-}}
            {{maimai Chart|maimai ORANGE|3|6|6|8|8|-}}
            {{maimai Chart|maimai ORANGE PLUS|3|6|6|8|8|11}}
            {{maimai Chart|maimai PiNK&rarr;FiNALE|3|6|6|7+|9|11}}
            {{maimai Chart|maimai でらっくす|-|6|6|7+|9|11}}
            {{maimai Chart|maimai でらっくす PLUS&rarr;Present|-|6|6|8|10|11+}}
            }}

            [[Category:?]]
            """
        ).strip()
    )


def test_example_Garden():
    """Garden was removed in ORANGE Plus, I test it for that"""
    with resources.open_binary(examples, "Garden.html") as f:
        tree = html.read(f)

    found = maimai_wiki_fc2.extract_song_info(tree)
    expected = model.Song(
        title=model.RomanizedString("Garden", None),
        artist=model.RomanizedString("May J.(cover)", None),
        bpm=Decimal("99"),
        standard_charts={
            model.Difficulty.EASY: model.Chart(
                level=model.LevelHistory(
                    changes={
                        model.Version.MAIMAI: model.Level(number=2, plus=False),
                    }
                ),
                note_counts=model.NoteCounts(
                    total=76, taps=66, holds=4, slides=4, breaks=2
                ),
                designer=None,
            ),
            model.Difficulty.BASIC: model.Chart(
                level=model.LevelHistory(
                    changes={
                        model.Version.MAIMAI: model.Level(number=4, plus=False),
                    }
                ),
                note_counts=model.NoteCounts(
                    total=103, taps=93, holds=4, slides=4, breaks=2
                ),
                designer=None,
            ),
            model.Difficulty.ADVANCED: model.Chart(
                level=model.LevelHistory(
                    changes={model.Version.MAIMAI: model.Level(number=5, plus=False)}
                ),
                note_counts=model.NoteCounts(
                    total=157, taps=146, holds=4, slides=6, breaks=1
                ),
                designer=None,
            ),
            model.Difficulty.EXPERT: model.Chart(
                level=model.LevelHistory(
                    changes={model.Version.MAIMAI: model.Level(number=7, plus=False)}
                ),
                note_counts=model.NoteCounts(
                    total=258, taps=210, holds=28, slides=12, breaks=8
                ),
                designer=None,
            ),
            model.Difficulty.MASTER: model.Chart(
                level=model.LevelHistory(
                    changes={
                        model.Version.MAIMAI: model.Level(number=10, plus=False),
                        model.Version.MAIMAI_PLUS: model.Level(number=9, plus=False),
                        model.Version.GREEN: model.Level(number=8, plus=False),
                    }
                ),
                note_counts=model.NoteCounts(
                    total=284, taps=214, holds=29, slides=24, breaks=17
                ),
                designer=None,
            ),
        },
        first_version=model.Version.MAIMAI,
        last_version=model.Version.ORANGE,
    )
    assert found == expected

    dumped = output.render_difficulty_and_note_counts(found)
    assert (
        dumped
        == dedent(
            """
            == Difficulty & Notecounts ==
            {{maimai Difficulty|_|orange}}
            {{Notecount Sources}}

            === maimai ===
            ==== Standard Charts ====
            {{maimai Chart Header|
            {{maimai Chart Notecounts|76|103|157|258|284}}
            {{maimai Chart Breakdown|66 / 4 / 4 / 2|93 / 4 / 4 / 2|146 / 4 / 6 / 1|210 / 28 / 12 / 8|214 / 29 / 24 / 17}}
            {{maimai Chart Designer|-|-}}
            {{maimai Chart|maimai|2|4|5|7|10}}
            {{maimai Chart|maimai PLUS|2|4|5|7|9}}
            {{maimai Chart|maimai GreeN&rarr;ORANGE|2|4|5|7|8}}
            }}
            """
        ).strip()
    )


def test_example_Love_or_Lies():
    """Love or Lies had a level change in Splash, I test it for that"""
    with resources.open_binary(examples, "Love or Lies.html") as f:
        tree = html.read(f)

    maimai_wiki_fc2.extract_song_info(tree)


def test_example_ZIGG_ZAGG():
    """ZIGG-ZAGG's standard difficulty history table has a でらっくす column
    for some reason"""
    with resources.open_binary(examples, "ZIGG-ZAGG.html") as f:
        tree = html.read(f)

    maimai_wiki_fc2.extract_song_info(tree)


def test_example_Justified():
    """Justified had a difficulty change in deluxe plus, which is written
    in an unexpected way on this page"""
    with resources.open_binary(examples, "Justified.html") as f:
        tree = html.read(f)

    maimai_wiki_fc2.extract_song_info(tree)
