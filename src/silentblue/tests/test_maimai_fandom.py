from decimal import Decimal
from importlib import resources
from textwrap import dedent

from silentblue import model, output
from silentblue.readers import html, maimai_fandom
from silentblue.tests.examples import maimai_fandom as examples


def test_example_come_again():
    with resources.open_binary(examples, "come_again.html") as f:
        tree = html.read(f)

    found = maimai_fandom.extract_song_info(tree)
    expected = model.Song(
        title=model.RomanizedString(
            "come again",
            "come again"
        ),
        artist=model.RomanizedString(
            "m-flo [cover]",
            "m-flo [cover]"
        ),
        bpm=Decimal("130"),
        standard_charts={
            model.Difficulty.EASY: model.Chart(
                level=model.LevelHistory({model.Version.MAIMAI: model.Level(2, False)}),
                note_counts=model.NoteCounts(68, 58, 5, 2, 3),
                designer=None,
            ),
            model.Difficulty.BASIC: model.Chart(
                level=model.LevelHistory(
                    {
                        model.Version.MAIMAI: model.Level(3, False),
                        model.Version.ORANGE: model.Level(5, False),
                    }
                ),
                note_counts=model.NoteCounts(105, 95, 6, 3, 1),
                designer=None,
            ),
            model.Difficulty.ADVANCED: model.Chart(
                level=model.LevelHistory(
                    {
                        model.Version.MAIMAI: model.Level(6, False),
                        model.Version.MAIMAI_PLUS: model.Level(5, False),
                        model.Version.GREEN: model.Level(6, False),
                    }
                ),
                note_counts=model.NoteCounts(118, 88, 21, 7, 2),
                designer=None,
            ),
            model.Difficulty.EXPERT: model.Chart(
                level=model.LevelHistory(
                    {
                        model.Version.MAIMAI: model.Level(7, False),
                        model.Version.MAIMAI_PLUS: model.Level(8, False),
                        model.Version.MURASAKI: model.Level(8, True),
                        model.Version.DELUXE_PLUS: model.Level(9, False),
                        model.Version.UNIVERSE_PLUS: model.Level(10, False),
                    }
                ),
                note_counts=model.NoteCounts(210, 139, 55, 14, 2),
                designer="譜面-100号",
            ),
            model.Difficulty.MASTER: model.Chart(
                level=model.LevelHistory(
                    {
                        model.Version.MAIMAI: model.Level(10, False),
                        model.Version.MAIMAI_PLUS: model.Level(9, False),
                        model.Version.PINK: model.Level(9, True),
                        model.Version.MURASAKI: model.Level(10, False),
                        model.Version.DELUXE_PLUS: model.Level(10, True),
                        model.Version.SPLASH: model.Level(11, True),
                        model.Version.UNIVERSE_PLUS: model.Level(12, False),
                    }
                ),
                note_counts=model.NoteCounts(264, 199, 45, 17, 3),
                designer="ニャイン",
            ),
        },
    )
    assert found == expected


def test_example_jelly():
    """Jelly has a utage chart, which I didn't prepare for at first"""
    with resources.open_binary(examples, "jelly.html") as f:
        tree = html.read(f)

    found = maimai_fandom.extract_song_info(tree)
    expected = [
        (
            model.UtageDifficulty.PARTY,
            model.UtageChart(
                note_counts=model.NoteCounts(346, 305, 17, 21, 3),
                designer=None,
                description=None,
                added_in=model.Version.FINALE,
            ),
        ),
    ]
    assert found.utage_charts == expected

    dumped = output.render_difficulty_and_note_counts(found)
    assert (
        dumped
        == dedent(
            """
            == Difficulty & Notecounts ==
            {{maimai Difficulty|_}}
            {{Notecount Sources}}

            === maimai ===
            ==== Standard Charts ====
            {{maimai Chart Header|
            {{maimai Chart Notecounts|60|108|162|256|346}}
            {{maimai Chart Breakdown|52 / 2 / 4 / 2|97 / 4 / 4 / 3|119 / 29 / 4 / 10|189 / 50 / 11 / 6|305 / 17 / 21 / 3}}
            {{maimai Chart Designer|[[Fumen -100 gou|譜面-100号]]|[[Nyain|ニャイン]]}}
            {{maimai Chart|maimai|1|3|6|7|10}}
            {{maimai Chart|maimai PLUS|1|3|6|7|9}}
            {{maimai Chart|maimai GreeN&rarr;GreeN PLUS|1|3|7|8|9}}
            {{maimai Chart|maimai ORANGE&rarr;ORANGE PLUS|2|4|7|8|9}}
            {{maimai Chart|maimai PiNK&rarr;PiNK PLUS|2|4|7|8|9+}}
            {{maimai Chart|maimai MURASAKi&rarr;MiLK|2|4|7|7+|10}}
            {{maimai Chart|maimai MiLK PLUS&rarr;FiNALE|2|4|7|7+|10+}}
            {{maimai Chart|maimai でらっくす|-|4|7|7+|10+}}
            {{maimai Chart|maimai でらっくす PLUS|-|4|7|8|10+}}
            {{maimai Chart|maimai でらっくす Splash&rarr;UNiVERSE|-|4|7|8|11+}}
            {{maimai Chart|maimai でらっくす UNiVERSE PLUS&rarr;Present|-|5|7|9|12}}
            }}

            ==== UTAGE (宴) ====
            {{UTAGE Header|
            {{UTAGE|maimai FiNALE|?|宴|-|346|305/17/21/3}}
            }}
            """
        ).strip()
    )
