<map version="freeplane 1.8.0">
<!--To view this file, download free mind mapping software Freeplane from http://freeplane.sourceforge.net -->
<node TEXT="Silentblue Data Importer" FOLDED="false" ID="ID_452131666" CREATED="1610381621610" MODIFIED="1656331039770" STYLE="bubble" SHAPE_HORIZONTAL_MARGIN="10.0 pt" SHAPE_VERTICAL_MARGIN="5.0 pt">
<font SIZE="18"/>
<hook NAME="MapStyle">
    <properties edgeColorConfiguration="#808080ff,#ff0000ff,#0000ffff,#00ff00ff,#ff00ffff,#00ffffff,#7c0000ff,#00007cff,#007c00ff,#7c007cff,#007c7cff,#7c7c00ff" associatedTemplateLocation="template:/standard-1.6-noEdgeColor.mm" fit_to_viewport="false"/>

<map_styles>
<stylenode LOCALIZED_TEXT="styles.root_node" STYLE="oval" UNIFORM_SHAPE="true" VGAP_QUANTITY="24.0 pt">
<font SIZE="24"/>
<stylenode LOCALIZED_TEXT="styles.predefined" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="default" COLOR="#000000" STYLE="fork" ID="ID_207122690">
<font NAME="SansSerif" SIZE="10" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.details"/>
<stylenode LOCALIZED_TEXT="defaultstyle.attributes">
<font SIZE="9"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.note" COLOR="#000000" BACKGROUND_COLOR="#ffffff" TEXT_ALIGN="LEFT"/>
<stylenode LOCALIZED_TEXT="defaultstyle.floating">
<edge STYLE="hide_edge"/>
<cloud COLOR="#f0f0f0" SHAPE="ROUND_RECT"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.selection" BACKGROUND_COLOR="#4e85f8" BORDER_COLOR_LIKE_EDGE="false" BORDER_COLOR="#4e85f8"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.user-defined" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="styles.topic" COLOR="#18898b" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subtopic" COLOR="#cc3300" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subsubtopic" COLOR="#669900">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.important" ID="ID_3752836">
<icon BUILTIN="yes"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.AutomaticLayout" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="AutomaticLayout.level.root" COLOR="#000000" STYLE="oval" SHAPE_HORIZONTAL_MARGIN="10.0 pt" SHAPE_VERTICAL_MARGIN="10.0 pt">
<font SIZE="18"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,1" COLOR="#0033ff">
<font SIZE="16"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,2" COLOR="#00b439">
<font SIZE="14"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,3" COLOR="#990000">
<font SIZE="12"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,4" COLOR="#111111">
<font SIZE="10"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,5"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,6"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,7"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,8"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,9"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,10"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,11"/>
</stylenode>
</stylenode>
</map_styles>
</hook>
<node TEXT="TODO" POSITION="right" ID="ID_118129909" CREATED="1656331000626" MODIFIED="1656331002047">
<node TEXT="metadonnées basiques" ID="ID_606614836" CREATED="1657957295828" MODIFIED="1657957304705">
<node TEXT="titre" ID="ID_705049263" CREATED="1657957307635" MODIFIED="1657957308727"/>
<node TEXT="artiste" ID="ID_762670923" CREATED="1657957308939" MODIFIED="1657957310351"/>
</node>
<node TEXT="wiki chinois" ID="ID_504105148" CREATED="1656331002231" MODIFIED="1656331007607">
<node TEXT="gérer une historique de niveau vide" FOLDED="true" ID="ID_1533001453" CREATED="1656331007916" MODIFIED="1657957254953">
<node TEXT="exemple avec conflict.html" ID="ID_1115812908" CREATED="1656850765776" MODIFIED="1657957254952">
<icon BUILTIN="button_ok"/>
</node>
</node>
<node TEXT="gérer les chart utage" ID="ID_1918463657" CREATED="1656850771279" MODIFIED="1656850774993">
<node TEXT="exemple avec" ID="ID_1065977349" CREATED="1656850778448" MODIFIED="1656850780787">
<node TEXT="jelly.html" ID="ID_1250277269" CREATED="1656850782400" MODIFIED="1656850784914"/>
<node TEXT="garakuta doll play" ID="ID_1478979138" CREATED="1656850785480" MODIFIED="1656850791810">
<node TEXT="pour quand y&apos;en a plusieurs" ID="ID_576228671" CREATED="1656850791992" MODIFIED="1656850797681"/>
</node>
</node>
</node>
</node>
<node TEXT="consolidation des infos de plusieurs sources" FOLDED="true" ID="ID_612356114" CREATED="1656331043228" MODIFIED="1656331055974">
<node TEXT="structurer ça pour pouvoir rajouter d&apos;autres sources" ID="ID_990868324" CREATED="1656331056528" MODIFIED="1656331065651">
<node TEXT="faire que l&apos;entrée à la main soit comprise comme une autre source ?" ID="ID_1611208438" CREATED="1656331067540" MODIFIED="1656331088213"/>
</node>
</node>
</node>
</node>
</map>
